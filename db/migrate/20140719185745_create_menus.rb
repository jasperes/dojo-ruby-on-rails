class CreateMenus < ActiveRecord::Migration
  def change
    create_table :menus do |t|
      t.text :dish
      t.integer :weekday

      t.timestamps
    end
  end
end
