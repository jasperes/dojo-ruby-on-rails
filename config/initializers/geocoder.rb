# config/initializers/geocoders.rb
Geocoder.configure(

  # geocoding service
  lookup: :google,
    
  # geocoding service request timeout
  timeout: 5,
    
  # default units
  units: :km

)