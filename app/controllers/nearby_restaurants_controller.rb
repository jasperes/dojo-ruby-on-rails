class NearbyRestaurantsController < ApplicationController

  def index
  end
  
  def get_by_location
    @restaurants = []
    @restaurants << '<b><i>Modo de teste!</i></b><br />'
    Restaurant.select(:name,:latitude,:longitude).each do |restaurant|
      #if Geocoder::Calculations.distance_between([params[:latitude],params[:longitude]], [restaurant.latitude, restaurant.longitude]) <= 10.0
      if Geocoder::Calculations.distance_between([98,9], [restaurant.latitude, restaurant.longitude]) <= 10.0
        @restaurants << restaurant.name
      end
    end
    render json: @restaurants
  end
end