json.array!(@menus) do |menu|
  json.extract! menu, :id, :dish, :weekday
  json.url menu_url(menu, format: :json)
end
