class Restaurant < ActiveRecord::Base

	validates :name, presence: true
	validates :price, presence: true, numericality: true
	validates :latitude, presence: true, numericality: true
	validates :longitude, presence: true, numericality: true

end
