class Menu < ActiveRecord::Base

	validates :dish, presence: true
	validates :weekday, numericality: {greater_than: 0, less_than: 8}

end
